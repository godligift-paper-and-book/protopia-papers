#   Заметки о протопии

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Лицензия Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />Это произведение доступно по <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">лицензии Creative Commons «Attribution-NonCommercial-NoDerivatives» («Атрибуция-Некоммерчески-БезПроизводных») 4.0 Всемирная</a>.

## О чем

Данный текст возник на основе размышлен и работы в Доме Протопии, Метаверситете, Тайном Маяке и смежных проектах. 

Это репозиторий заметок о Протопии.

Здесь описывается как должно быть, а не как есть.

##  Build Instructions

With pip and stuff (requires Python 3.6+, Pandoc, and TeXLive):

```bash
$ pip install -r requirements.txt
# Site:
$ foliant make site
# PDF:
$ foliant make pdf
```
